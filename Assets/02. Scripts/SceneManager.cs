﻿using UnityEngine;
using System.Collections;


public class SceneManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//Application.LoadLevel ("Scene_SimonSays");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void go2Scene(string scene) {
		Application.LoadLevel (scene);
	}

	public void exitGame() {
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#elif UNITY_STANDALONE
			Application.Quit();
		#endif
	}
}
