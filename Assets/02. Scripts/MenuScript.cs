﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour {
	public float rotationSpeed = 200.0f;

	// Use this for initialization
	void Start () {
		Debug.Log("MenuScript_Start");
	}
	
	// Update is called once per frame
	void Update () {
		float finalRotationSpeed = rotationSpeed * Time.deltaTime;
		transform.Rotate (0.0f, finalRotationSpeed, 0.0f);
	}
}
