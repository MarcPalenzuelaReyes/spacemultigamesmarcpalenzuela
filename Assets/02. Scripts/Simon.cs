﻿using UnityEngine;
using System.Collections;

public class Simon : MonoBehaviour {
	public GameObject[] lights;
	public float timePerLight = 3.0f;
	public float timeWithoutLight = 1.0f;
	public KeyCode key = KeyCode.A;
	public KeyCode key2 = KeyCode.B;

	//private float timer = 0;
	private bool active = false;

	// Use this for initialization
	void Start () {
		closeLights ();
	}

	void closeLights() {
		foreach (GameObject l in lights) {
			l.SetActive(false);
		}
		Invoke ("openRandomLight", timeWithoutLight);
	}

	// Update is called once per frame
	void Update () {

		/*timer += Time.deltaTime;
		if (active == false) {
			if (timer >= timeWithoutLight) {
				openRandomLight();
				timer = 0;
				active = true;
			}
		} else {
			if (timer >= timePerLight) {
				closeLights ();
				timer = 0;
				active = false;
			}
		}*/
	}

	void openRandomLight() {
		int n = Random.Range (0, lights.Length);
		lights[n].SetActive (true);
		Invoke ("closeLights", timePerLight);
	}
}
